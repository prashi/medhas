const express = require('express');
const pool = require('./database');
const nodemailer = require("nodemailer");

const router = express.Router();
var smtpTransport = nodemailer.createTransport("SMTP",{
    Service: "Office365",
	auth: {
		user: "prashanth@moviequ.com",
		pass: "Inter123$"
    }
});
var no_rply = "Prashanth<prashanth@moviequ.com>";

function randomString(length, chars) {
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
}

router.post("/sendOTP", (req, res) => {

	var details=req.body;
	var rString = randomString(6, '0123456789');

	var query = `SELECT name, email FROM login WHERE email="${details.email}"`;
    pool.query(query, (err, result_2) => {
		if(result_2 && result_2.length > 0){

			mailOptions={
				from: no_rply,
				to : details.email,
				subject : "Verifiy your email ID",
				html : "Hi "+result_2[0].name+",<br><br>Thank you for registering with us. <br><br>Use the below verification code to activate your account "+rString+"<br><br>Warm Regards, <br>Medha",
			}
			smtpTransport.sendMail(mailOptions, function(error, response){
				if(!error){
					pool.query(`UPDATE login SET register_code="${rString}", account_verified=1 WHERE email="${details.email}"`, (err, result) => {
						if (err){
							var retn_details = {};
							retn_details.status="error";
							retn_details.msg = "rror in updating user details";
							res.send(retn_details);
						}else{
							var retn_details = {};
							retn_details.status="success";
							retn_details.msg = "Updated";
							res.send(retn_details);
						}
					})
				}else{
					var retn_details = {};
					retn_details.status="error";
					retn_details.msg = "Failed to send OTP";
					res.send(retn_details);
				}
			});
		}
	});
});

router.post("/verifyAccount", (req, res) => {

	var details=req.body;
	var query = `SELECT email FROM login WHERE email="${details.email}" AND register_code="${details.code}"`;
    pool.query(query, (err, result_2) => {
		if(result_2 && result_2.length > 0){
			var update_query = `UPDATE login SET account_verified = 1 WHERE email="${details.email}" AND register_code="${details.code}"`;
			pool.query(update_query, (err, result) => {
				if (err){
					var retn_details = {};
					retn_details.status="error";
					retn_details.msg = "Error while verifying your account.";
					res.send(retn_details);
				}else{
					var retn_details = {};
					retn_details.status="success";
					retn_details.msg = "Account verified";
					res.send(retn_details);
				}
			});
		}else{
			
			var retn_details = {};
			retn_details.status="error";
			retn_details.msg = "Invalid Email or Code";
			res.send(retn_details);
		}
	})
});

router.post("/updatePassword", (req, res) => {

	var details=req.body;
	var update_query = `UPDATE login SET password = "${details.password}" WHERE email="${details.email}" AND account_verified=1`;
	pool.query(update_query, (err, result_2) => {
		if(result_2){
			var retn_details = {};
			retn_details.status="success";
			retn_details.msg = "valid User";
			retn_details.data = result_2;
			res.send(retn_details);
		}else{
			var retn_details = {};
			retn_details.status="error";
			retn_details.msg = "Invalid Email";
			res.send(retn_details);
		}
	})
});

router.post("/accountLogin", (req, res) => {

	var details=req.body;
	var query = `SELECT st.id, lg.name, lg.email, lg.mobile, lg.account_verified FROM login lg JOIN students st ON lg.id=st.parent_id WHERE email="${details.email}" AND password="${details.password}"`;
    pool.query(query, (err, result_2) => {
		if(result_2 && result_2.length > 0){
			if(result_2[0].account_verified == 1){
				var retn_details = {};
				retn_details.status="success";
				retn_details.msg = "valid User";
				retn_details.details = result_2;
				res.send(retn_details);
			}else{
				var retn_details = {};
				retn_details.status="error";
				retn_details.msg = "Your account is not Verified.";
				res.send(retn_details);
			}
		}else{
			var retn_details = {};
			retn_details.status="error";
			retn_details.msg = "Invalid Email or Password";
			res.send(retn_details);
		}
	})
});

router.post("/getAttentionByMonth", (req, res) => {

	var details=req.body;
	var num_of_working = 30;
	var query = `SELECT al.class_id, DATE_FORMAT(al.date,'%Y-%m-%d') as date, al.reason FROM absent_leaves al WHERE al.month=${details.month} AND al.year=${details.year} AND student_id=${details.student_id}`;
	console.log(query);
    pool.query(query, (err, result_2) => {
		if(result_2 && result_2.length > 0){
			
			var class_id = result_2[0].class_id;
			var total_working_days = 310;
			var query = `SELECT DATE_FORMAT(hd.date,'%Y-%m-%d') as date, hd.reason, total_working_days FROM holidays_dates hd JOIN attendance_master am ON hd.class_id=am.class_id WHERE hd.month=${details.month} AND hd.year=${details.year} AND hd.class_id=${class_id};`;
			pool.query(query, (err, result_3) => {
				
				var absent_details = [];
				for(var i=0; i<result_2.length; i++){
					var temp = {};
					temp.date = result_2[i].date;
					temp.reason = result_2[i].reason;
					temp.is_absent_holiday = 1;
					absent_details.push(temp);
				}
				if(result_3 && result_3.length>0){
					for(var i=0; i<result_3.length; i++){
						var temp = {};
						temp.date = result_3[i].date;
						temp.reason = result_3[i].reason;
						temp.is_absent_holiday = 2;
						absent_details.push(temp);
					}
					total_working_days = result_3[0].total_working_days;
				}
				var result_retn = {};
				result_retn.days_present = num_of_working - (result_2.length + result_3.length);
				result_retn.days_absent = result_2.length;
				result_retn.days_holiday = result_3.length;
				result_retn.total_working_days = total_working_days;
				result_retn.absent_details = absent_details;
				var retn_details = {};
				retn_details.status="success";
				retn_details.msg = "valid User";
				retn_details.details = result_retn;
				res.send(retn_details);
			});
		}else{
			var result_retn = {};
			result_retn.days_present = num_of_working;
			result_retn.days_absent = 0;
			result_retn.days_holiday = 0;
			result_retn.total_working_days = total_working_days;
			result_retn.absent_details = [];
			var retn_details = {};
			retn_details.status="success";
			retn_details.msg = "valid User";
			retn_details.details = result_retn;
			res.send(retn_details);
		}
	})
});
module.exports = router;