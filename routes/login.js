const express = require('express');
const pool = require('./database');
//const config = require('config');
const nodemailer = require("nodemailer");

const router = express.Router();
/*var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
	auth: {
		user: "shreyas.r830@gmail.com",
		pass: "Inter123$"
    }
});
var no_rply = "Prashanth<shreyas.r830@gmail.com>";*/
var smtpTransport = nodemailer.createTransport("SMTP",{
    Service: "Office365",
	auth: {
		user: "prashanth@moviequ.com",
		pass: "Inter123$"
    }
});
var no_rply = "Prashanth<prashanth@moviequ.com>";


router.post("/registerNew", (req, res) => {

	var details=req.body;
	
	var query = `SELECT email FROM login WHERE email="${details.email}"`;
    pool.query(query, (err, result_2) => {
		if(result_2 && result_2.length > 0){
			var retn_details = {};
			retn_details.err_msg = "Email already exists";
			res.send(retn_details);
		}else{

			var temp = {};
			temp.name = details.userName;
			temp.email = details.email;
			temp.mobile = details.phone_num;
			temp.roles = details.roles;
			temp.account_verified = 0;

			mailOptions={
				from: no_rply,
				to : details.email,
				subject : "You are Registered",
				html : "Hi "+details.userName+",<br><br>Thank you for registering with us. <br><br>Please use our app to sign up and proceed further<br><br>Warm Regards, <br>Medha",
			}
			smtpTransport.sendMail(mailOptions, function(error, response){
				if(!error){
					pool.query('insert into login SET ?', temp, (err, result) => {
						if (err){
							var retn_details = {};
							retn_details.err_msg = "Error in updating user details";
							res.send(retn_details);
						}else{
							var temp1 = {};
							temp1.role_no = details.role_no;
							temp1.parent_id = result.insertId;
							temp1.first_name = details.student_firstname;
							temp1.last_name = details.student_lastname;
							temp1.class = details.standard;
							pool.query('insert into students SET ?', temp1, (err, result_1) => {
								if (err){
									var retn_details = {};
									retn_details.err_msg = "Error in updating Student details";
									res.send(retn_details);
								}else{
									var retn_details = {};
									retn_details.message = "Updated";
									res.send(retn_details);
								}
							})
						}
					})
				}else{
					var retn_details = {};
					retn_details.err_msg = "Sending mail failed";
					res.send(retn_details);
				}
			});
		}
	});
});

router.post("/portalLogin", (req, res) => {

	var details=req.body;
	if(details.user_id == "admin" && details.password=="admin321"){
		var retn_details = {};
		retn_details.status="success";
		retn_details.msg = "valid User";
		res.send(retn_details);
	}else{
		var retn_details = {};
		retn_details.status="error";
		retn_details.msg = "Invalid User Id or Password";
		res.send(retn_details);
	}
});

module.exports = router;