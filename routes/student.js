const express = require('express');
const pool = require('./database');
//const config = require('config');

const router = express.Router();

router.post("/studentByClass", (req, res) => {

	var details=req.body;
	
	var query = `SELECT role_no, first_name, last_name FROM students WHERE class=${details.standard}`;
	pool.query(query, (err, result_1) => {
		if (err){
			var retn_details = {};
			retn_details.err_msg = "Error in updating Student details";
			res.send(retn_details);
		}else{
			res.send(result_1);
		}
	})
});

module.exports = router;