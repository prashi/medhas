const express = require('express');
const pool = require('./database');
//const config = require('config');

const router = express.Router();

router.post("/addAbsentees", (req, res) => {

	var details=req.body;
	
	var extract_month_year = new Date(details.absent_date);
	var month = (extract_month_year.getMonth())+1;
	var year = extract_month_year.getFullYear();
	var temp = {};
	temp.student_id = details.students;
	temp.class_id = details.standard;
	temp.month = month;
	temp.year = year;
	temp.date = details.absent_date;
	temp.reason = details.absent_reason;
	temp.is_absent_leave = 1;
	console.log(temp);
	pool.query('insert into absent_leaves SET ?', temp, (err, result) => {
		console.log(err);
		console.log(result);
		if (err){
			var retn_details = {};
			retn_details.err_msg = "Error in updating Records details";
			res.send(retn_details);
		}else{
			res.send(result);
		}
	})
});

module.exports = router;