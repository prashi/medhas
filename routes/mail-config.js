const nodemailer = require("nodemailer");
const config = require('config');

const smtpTransport = nodemailer.createTransport({
    service: 'gmail',
	auth: {
		user: config.get("mail.user"),
		pass: config.get("database.pwd"),
	}
});

const pool = mysql.createPool({
    connectionLimit: 500,
    host: config.get("database.host"),
    user: config.get("database.user"),
    password: config.get("database.pwd"),
    database: config.get("database.db")
});

pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
    }
    if (connection) connection.release()
    return
})
module.exports = pool