const express = require('express');
const app = express();

const login = require('./routes/login');
const ws = require('./routes/ws');
const student = require('./routes/student');
const attendance = require('./routes/attendance');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static('views'));

app.use('/login', login);
app.use('/student', student);
app.use('/attendance', attendance);

app.use('/ws', ws);

const port = process.env.port || 9090;
app.listen(port, () => console.log(`listening on port ${port}`));