$('#registerUser').click(function () {
    if ($('#fullname').val() == "") {
    	toastr.error('Please Enter FullName');
        return -1;
    }
    if ($('#email').val() == "") {
    	toastr.error('Please Enter Proper Email Id');
        return -1;
    }
    if ($('#phone_num').val() == "") {
    	toastr.error('Please Enter 10 Digit Phone Number');
        return -1;
    }
    var authenticate = {};
    authenticate.userName = $('#fullname').val();
	authenticate.email = $('#email').val();
	authenticate.phone_num = $('#phone_num').val();
    authenticate.roles = $('#roles').val();
    authenticate.student_firstname = $('#student_firstname').val();
	authenticate.student_lastname = $('#student_lastname').val();
	authenticate.role_no = $('#role_no').val();
    authenticate.standard = $('#standard').val();
    $.ajax({
        url: '/login/registerNew',
        data: JSON.stringify(authenticate),
        type: "POST",
        contentType: 'application/json',
        success: function successmsg(data) {
			alert(JSON.stringify(data));
			window.location = window.location;
		}
    });
})
document.onkeydown = function (e) {
    var keyCode = e.keyCode;
    if (keyCode == 13) {
        $('#registerUser').trigger("click");
    }
};
