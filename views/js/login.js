$('#authenticateLogin').click(function () {
    if ($('#user_id').val() == "") {
    	toastr.error('Please Enter User Id');
        return -1;
    }
    if ($('#password').val() == "") {
    	toastr.error('Please Enter Password');
        return -1;
    }
    var authenticate = {};
    authenticate.user_id = $('#user_id').val().toLowerCase();
    authenticate.password = $('#password').val();
    $.ajax({
        url: '/login/portalLogin',
        data: JSON.stringify(authenticate),
        type: "POST",
        contentType: 'application/json',
        success: function successmsg(data) {
            if (data.status=="success") {
                sessionStorage.setItem("login_status", "success");
                window.location = "register.html"
            } else {
				toastr.error(data.msg);
            }
        }
    });
})
document.onkeydown = function (e) {
    var keyCode = e.keyCode;
    if (keyCode == 13) {
        $('#authenticateLogin').trigger("click");
    }
};
