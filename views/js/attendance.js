$('#standard').change(function () {
    var temp = {};
    temp.standard = $('#standard').val();
    $.ajax({
        url: '/student/studentByClass',
        data: JSON.stringify(temp),
        type: "POST",
        contentType: 'application/json',
        success: function successmsg(data) {
			$('#students').html('');
			if(data.length>0){
				for(var i=0; i<data.length; i++){
					var fl_name = "";
					if(data[i].first_name){
						fl_name = fl_name +data[i].first_name;
					}
					if(data[i].last_name){
						fl_name = fl_name +" "+data[i].last_name;
					}
					$('#students').append('<option value="'+data[i].role_no+'">'+fl_name+'</option>')
				}
			}
		}
    });
})
$('#submitDetails').click(function (){
    var temp = {};
    temp.standard = $('#standard').val();
    temp.students = $('#students').val();
    temp.absent_date = $('#absent_date').val();
    temp.absent_reason = $('#absent_reason').val();
    $.ajax({
        url: '/attendance/addAbsentees',
        data: JSON.stringify(temp),
        type: "POST",
        contentType: 'application/json',
        success: function successmsg(data) {
			alert(JSON.stringify(data));
			window.location = window.location;
		}
    });
})
    var temp = {};
    temp.standard = $('#standard').val();
    $.ajax({
        url: '/student/studentByClass',
        data: JSON.stringify(temp),
        type: "POST",
        contentType: 'application/json',
        success: function successmsg(data) {
			$('#students').html('');
			if(data.length>0){
				for(var i=0; i<data.length; i++){
					var fl_name = "";
					if(data[i].first_name){
						fl_name = fl_name +data[i].first_name;
					}
					if(data[i].last_name){
						fl_name = fl_name +" "+data[i].last_name;
					}
					$('#students').append('<option value="'+data[i].role_no+'">'+fl_name+'</option>')
				}
			}
		}
    });

	$('#absent_date').daterangepicker({
		singleDatePicker: true,
		locale: {
			format: 'YYYY-MM-DD'
		}
	})
